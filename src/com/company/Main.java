package com.company;

import com.company.subject.*;

import java.io.IOException;
import java.util.Arrays;

public class Main {

    public static void main(String[] args) throws IOException {
        Border border = new Border(3);

        Player player1 = new Player(new X(), "Lalka");
        Player player2 = new Player(new O(), "Palka");

        Game game = new Game(border, player1, player2);
        game.newGame();
    }
}
