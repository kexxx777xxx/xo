package com.company.subject;

import static org.junit.Assert.*;

/**
 * Created by kex1 on 4/9/15.
 */
public class GameTest {

    @org.junit.Before
    public void setUp() throws Exception {

    }

    @org.junit.After
    public void tearDown() throws Exception {

    }

    @org.junit.Test
    public void testNewGame() throws Exception {

    }

    @org.junit.Test
    public void testWinHorizontalPlayer1() throws Exception {
        Border border = new Border(3);

        Player player1 = new Player(new X(), "Lalka");
        Player player2 = new Player(new O(), "Palka");
        border.set(0, 0, player1);
        border.set(0, 1, player1);
        border.set(0, 2, player1);

        Game game = new Game(border, player1, player2);
        assertTrue(game.win());
    }


    @org.junit.Test
    public void testWinVerticalPlayer1() throws Exception {
        Border border = new Border(3);

        Player player1 = new Player(new X(), "Lalka");
        Player player2 = new Player(new O(), "Palka");
        border.set(0, 0, player1);
        border.set(1, 0, player1);
        border.set(2, 0, player1);

        Game game = new Game(border, player1, player2);
        assertTrue(game.win());
    }


    @org.junit.Test
    public void testWinDiagonalLeftPlayer1() throws Exception {
        Border border = new Border(3);

        Player player1 = new Player(new X(), "Lalka");
        Player player2 = new Player(new O(), "Palka");
        border.set(1, 1, player1);
        border.set(2, 2, player1);
        border.set(3, 3, player1);

        Game game = new Game(border, player1, player2);
        assertTrue(game.win());
    }

    @org.junit.Test
    public void testWinDiagonalRightPlayer1() throws Exception {
        Border border = new Border(3);

        Player player1 = new Player(new X(), "Lalka");
        Player player2 = new Player(new O(), "Palka");
        border.set(0, 2, player1);
        border.set(1, 1, player1);
        border.set(2, 0, player1);

        Game game = new Game(border, player1, player2);
        assertTrue(game.win());
    }
}