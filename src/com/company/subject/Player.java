package com.company.subject;

public class Player {
    N value;
    String name;
    boolean win = false;

    public Player(N value, String name) {
        this.value = value;
        this.name = name;
    }

    public N getValue() {
        return value;
    }

    public String getName() {
        return name;
    }

    public void setWin(boolean win) {
        this.win = win;
    }

    public boolean getWin() {
        return win;
    }

    public void showWin() {
        if (this.win) {
            System.out.println("Player with " + this.getValue().getClass() + "Win");
        }
    }
}
