package com.company.subject;

import java.io.Console;
import java.io.IOException;
import java.util.Scanner;

/**
 * Created by kex1 on 4/8/15.
 */
public class Border {
    N[][] borderSize;
    public int limit;

    public Border(int limit) {
        this.limit = limit;
        this.clearBoard();
    }

    public void render() {
        for (N[] border : borderSize) {
            System.out.print("[");
            for (N b : border) {
                System.out.print(b.toString() + ",");
            }
            System.out.println("]");
        }
    }

    public boolean set(int x, int y, Player player) {
        if (x < limit && y < limit && borderSize[x][y] instanceof Empty) {
            borderSize[x][y] = player.getValue();
            return true;
        }

        return false;
    }

    public boolean checkWin() {
        return false;
    }

    public void clearBoard() {
        borderSize = new N[limit][limit];

        for (int i = 0; i < limit; i++) {
            for (int j = 0; j < limit; j++) {
                borderSize[i][j] = new Empty();
            }
        }

    }
}
