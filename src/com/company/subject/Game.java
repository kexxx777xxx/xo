package com.company.subject;

import java.util.Scanner;

/**
 * Created by kex1 on 4/8/15.
 */
public class Game {
    Border border;
    Player[] players;
    private boolean movePlayer = true;

    public Game(Border border, Player player1, Player player2) {
        this.border = border;
        this.players = new Player[]{player1, player2};
    }

    public void newGame() {
        border.clearBoard();
        border.render();

        while (!win()) {
            Player currentPlayer = players[movePlayer ? 0 : 1];
            Scanner scanner = new Scanner(System.in);

            System.out.print("Move player " + currentPlayer.getName());
            String input = scanner.next();
            String[] xy = input.split(",", 2);

            int x = Integer.parseInt(xy[0]) - 1;
            int y = Integer.parseInt(xy[1]) - 1;

            if (border.set(x, y, currentPlayer)) {
                movePlayer = !movePlayer;
            }

            border.render();
        }

        border.render();
        this.players[0].showWin();
        this.players[1].showWin();
    }

    public boolean win() {
        int lim = border.limit;

        boolean winDLeft1 = true, winDRight1 = true;
        boolean winDLeft2 = true, winDRight2 = true;

        for (int i = 0, j = lim - 1; i < lim; i++, j--) {
            winDLeft1 = winDLeft1 && border.borderSize[i][i].getClass() == (this.players[0].getValue().getClass());
            winDRight1 = winDRight1 && border.borderSize[i][j].getClass() == (this.players[1].getValue().getClass());

            winDLeft2 = winDLeft2 && border.borderSize[i][i].getClass() == (this.players[0].getValue().getClass());
            winDRight2 = winDRight2 && border.borderSize[i][j].getClass() == (this.players[1].getValue().getClass());
        }

        this.players[0].setWin(winDLeft1 || winDRight1);
        this.players[1].setWin(winDLeft2 || winDRight2);

        if (this.players[0].getWin() || this.players[1].getWin()) {
            return true;
        }

        for (int i = 0; i < lim; i++) {
            boolean winH1 = true, winV1 = true;
            boolean winH2 = true, winV2 = true;

            for (int j = 0; j < lim; j++) {
                winH1 = winH1 && border.borderSize[i][j].getClass() == this.players[0].getValue().getClass();
                winV1 = winV1 && border.borderSize[j][i].getClass() == this.players[0].getValue().getClass();

                winH2 = winH2 && border.borderSize[i][j].getClass() == this.players[1].getValue().getClass();
                winV2 = winV2 && border.borderSize[j][i].getClass() == this.players[1].getValue().getClass();
            }

            this.players[0].setWin(winH1 || winV1);
            this.players[1].setWin(winH2 || winV2);

            if (this.players[0].getWin() || this.players[1].getWin()) {
                return true;
            }
        }

        return false;
    }
}
